const jwt = require("jsonwebtoken")
const config = require("../config/config")
const { error } = require("../network/response")

function sign(data) {
    return jwt.sign(data, config.api.jwtSecret)
}

function verify(token) {
    try {
        return jwt.verify(token, config.api.jwtSecret)
    } catch (error) {
        throw { message: "TOKEN_INVALID", statusCode: 500 }
    }
}

const check = {
    own: function (req, res, owner) {
        const tokenDecoded = decodeHeader(req, res)
        //Check is owner
        if (tokenDecoded.id !== owner) {
            error(req, res, "NOT_ALLOWED_ACTION", 405)
        }
    },
    permission: function (req, res, roles) {
        const tokenDecoded = decodeHeader(req, res)
        if (!tokenDecoded) {
            throw { message: "NOT_ALLOWED_ACTION 1", statusCode: 405 }
        }
        if (!roles.includes(tokenDecoded.role)) {
            throw { message: "NOT_ALLOWED_ACTION 2", statusCode: 405 }
        }
    },
    role: function (req, res) {
        try {
            const decode = decodeHeader(req, res, "ALLOWED_ALL")
            console.log("check role", decode)
        } catch (error) {
            console.log("error check role")
        }
    },
    logged: function (req, res) {
        decodeHeader(req, res, "ALLOWED_AUTH")
    },
}

function getToken(auth) {
    if (!auth) {
        throw { message: "NOT_TOKEN", statusCode: 400 }
    }

    if (auth.indexOf("Bearer ") === -1) {
        throw { message: "TOKEN_NOT_VALID", statusCode: 405 }
    }

    let token = auth.replace("Bearer ", "")
    return token
}

function decodeHeader(req, res, typeAccess) {
    console.log("decodeHeader")
    const authorization = req.headers.authorization || ""
    if (
        typeAccess === "ALLOWED_ALL" &&
        (!authorization || authorization === "")
    ) {
        req.user = { role: "none" }
        return { role: "none" }
    }
    const token = getToken(authorization)
    const decoded = verify(token)
    req.user = decoded
    return decoded
}

module.exports = {
    sign,
    check,
}
