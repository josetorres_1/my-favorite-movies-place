const express = require("express")
const bodyParser = require("body-parser")
const cors = require("cors")
const dotenvt = require("dotenv")
dotenvt.config()

const config = require("./config/config")
const app = express()

const movie = require("./components/movie/routes")
const like = require("./components/like/routes")
const user = require("./components/user/routes")
const sale = require("./components/sale/routes")
const rent = require("./components/rent/routes")
const auth = require("./components/auth/routes")
const { error } = require("./network/response")

//middleware
app.use(bodyParser.json())
app.use(cors())


//rutes
app.use("/api/movie", movie)
app.use("/api/rent", rent)
app.use("/api/auth", auth)
app.use("/api/user", user)
app.use("/api/like", like)
app.use("/api/sale", sale)

app.get("/", (req, res) => {
    res.status(200).send("Hello World")
})

//Middleware error handling
app.use((err, req, res, next) => {
    console.error("err", err.message)
    if (!err.message) err.message = "INTERNAL_ERROR"
    if (!err.statusCode) err.statusCode = 500
    error(req, res, err.message, err.statusCode)
})

app.listen(config.api.port, () => {
    console.log("API running on", config.api.port)
})
