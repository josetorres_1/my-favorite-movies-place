module.exports = {
    api: {
        port: process.env.PORT || 3000,
        jwtSecret: process.env.JWT_SECRET || "1el21Kwp5X"
    },
}
