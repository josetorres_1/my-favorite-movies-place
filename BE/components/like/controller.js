const { PrismaClient } = require("@prisma/client")
const { check } = require("../../security")

const prisma = new PrismaClient()

async function checkLike(userId, movieId) {
    return await prisma.likesOnMoviesByUsers.findUnique({
        where: {
            movieId_userId: {
                movieId,
                userId,
            },
        },
    })
}

async function like(userId, movieId) {
    const existingLike = await checkLike(userId, movieId)
    if (existingLike) {
        await prisma.likesOnMoviesByUsers.delete({
            where: {
                movieId_userId: {
                    movieId,
                    userId,
                },
            },
        })
    } else {
        await prisma.likesOnMoviesByUsers.create({
            data: { movieId, userId },
        })
    }
    const result = await prisma.likesOnMoviesByUsers.count({
        where: { movieId: { equals: movieId } },
    })

    return result || 0
}

// async function removeLike(data) {
//     const { movieId, userId } = data

//     const result = await prisma.likesOnMoviesByUsers.count({
//         where: { movieId: { equals: movieId } },
//     })

//     return result
// }

module.exports = {
    like,
}
//add like
//remove like
