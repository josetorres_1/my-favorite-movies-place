const auth = require("../../security/index")

module.exports = function checkAuth(action) {
    function middleWare(req, res, next) {
        switch (action) {
            case "LOGGED":
                auth.check.logged(req, res)
                next()
                break
            default:
                next()
                break
        }
    }
    return middleWare
}
