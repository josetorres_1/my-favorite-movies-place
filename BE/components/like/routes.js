const { Router } = require("express")
const express = require("express")

const secure = require("./secure")
const response = require("../../network/response")
const controller = require("./index")

const router = express.Router()

router.get("/:id", secure("LOGGED"), async (req, res) => {
    try {
        const userId = req.user.id
        const movieId = parseInt(req.params.id)
        const like = await controller.like(userId, movieId)
        response.success(req, res, like, 201)
    } catch (error) {
        console.error(error);

        response.error(req, res, error.message, 500)
    }
})

module.exports = router
