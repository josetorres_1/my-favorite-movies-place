const { PrismaClient } = require("@prisma/client")
const bycrypt = require("bcrypt")

const prisma = new PrismaClient()

async function addUser(data) {
    try {
        const { email, name, password, newPassword, role, id } = data
        const encryptedPassword = await bycrypt.hash(password, 5)

        const user = await prisma.user.upsert({
            where: { id: id | null },
            create: {
                email,
                name,
                role,
                auth: { create: { password: encryptedPassword } },
            },
            update: {
                email,
                name,
                role,
                auth: { update: { password: encryptedPassword } },
            },
        })
        return user
    } catch (error) {
        console.log(error)
        throw new Error(error.message)
    }
}

async function getUser(data) {
    const { email, password } = data
    const user = prisma.client.findUnique({
        where: {
            email,
            password,
        },
        include: { likes: true, CustomerRental: true, CustomerInvoice: true },
    })

    return user
}

async function getUserById(id) {
    try {
        const user = await prisma.user.findUnique({
            where: {
                id: parseInt(id),
            },
            include: {
                likes: true,
                customerRental: true,
                customerInvoice: true,
            },
        })

        return user
    } catch (error) {
        console.log(error)
        throw new Error(error.message)
    }
}

async function getUsers(data) {
    let arg = data || {}
    try {
        const user = await prisma.user.findMany({
            ...arg,
        })

        return user
    } catch (error) {
        console.log(error.message)
        throw error
    }
}

async function updateUser(data) {
    const { id, role } = data
    const user = await prisma.user.update({
        where: { id },
        data: { role },
    })

    return user
}

module.exports = {
    getUser,
    getUserById,
    addUser,
    getUsers,
    updateUser,
}
