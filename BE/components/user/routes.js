const { Router } = require("express")
const express = require("express")

const response = require("../../network/response")
const controller = require("./index")
const secure = require("./secure")

const router = express.Router()

router.post("/", async (req, res) => {
    try {
        const { email, name, password, id, role } = req.body
        const user = await controller.addUser({
            email,
            name,
            password,
            id,
            role,
        })
        response.success(req, res, user, 201)
    } catch (error) {
        response.error(req, res, error.message, 500)
    }
})

router.put("/admin/user/:id", secure("UPSERT"), async (req, res) => {
    try {
        const id = parseInt(req.params.id)
        const { role } = req.body
        console.log({ id, role })
        const user = await controller.updateUser({
            id,
            role,
        })
        response.success(req, res, user, 201)
    } catch (error) {
        console.error(error)
        response.error(req, res, error.message, 500)
    }
})

router.get("/", secure("UPSERT"), async (req, res) => {
    try {
        const users = await controller.getUsers()
        response.success(req, res, users, 200)
    } catch (error) {
        console.error(error)
        response.error(req, res, error.message, 500)
    }
})

router.get("/:id", async (req, res) => {
    try {
        const user = await controller.getUserById(req.params.id)
        response.success(req, res, user, 200)
    } catch (error) {
        response.error(req, res, error.message, 500)
    }
})

module.exports = router
