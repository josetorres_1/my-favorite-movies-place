const { PrismaClient } = require("@prisma/client")
const bcrypt = require("bcrypt")
const { sign } = require("../../security")

const prisma = new PrismaClient()

async function login(data) {
    const { email, password } = data
    console.log(data)
    const user = await prisma.auth.findMany({
        where: { user: { email } },
        select: { password: true, user: { select: { id: true, role: true } } },
    })
    if (user && user[0]) {
        return bcrypt.compare(password, user[0].password).then((result) => {
            if (result === true) {
                return sign(user[0].user)
            } else {
                throw new Error("Información invalida")
            }
        })
    } else {
        throw new Error("NOT_USER_FOUND")
    }
}

module.exports = {
    login,
}
