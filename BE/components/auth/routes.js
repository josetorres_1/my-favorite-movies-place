const { Router } = require("express")
const express = require("express")

const response = require("../../network/response")
const controller = require("./index")

const router = express.Router()

router.post("/login", async (req, res) => {
    try {
        const token = await controller.login({
            email: req.body.email,
            password: req.body.password,
        })
        if (token) {
            response.success(req, res, token, 200)
        } else {
            response.error(req, res, "Not found", 400)
        }
    } catch (error) {
        response.error(req, res, error.message, 500)
    }
})

module.exports = router
