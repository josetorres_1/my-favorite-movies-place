const { PrismaClient } = require(".prisma/client")

const prisma = new PrismaClient()

// Todo review upsert with admin relationship
//create rent
async function upsertRent(data) {
    const {
        id,
        movieId,
        customerId,
        adminId,
        rentalPrice,
        dateToBeReturned,
        dateReturned,
        delayedReturned,
    } = data
    const rent = await prisma.rental.upsert({
        where: { id: id | null },
        create: {
            movieId,
            customerId,
            // admin: {connectOrCreate: {
            //     where: {id}
            // }},
            rentalPrice,
            dateToBeReturned: new Date(dateToBeReturned).toISOString(),
            delayedReturned: false,
        },
        update: {
            dateReturned,
            delayedReturned,
        },
    })
    console.log(rent)
    return rent
}
//get rents

async function getRents(data, userId) {
    const rents = await prisma.rental.findMany({
        include: { movie: true, customer: true },
    })

    return rents
}

async function getRentsByUser(data, userId) {
    const rents = await prisma.rental.findMany({
        where: {
            customer: { id: userId | null },
        },
    })

    return rents
}
//get rent

async function getRent(id) {
    const rent = await prisma.rental.findUnique({
        where: { id: parseInt(id) },
        include: { customer: true, admin: true, movie: true },
    })

    return rent || []
}

//update rent

module.exports = {
    getRents,
    getRent,
    upsertRent,
    getRentsByUser,
}
