const { PrismaClient } = require("@prisma/client")

const prisma = new PrismaClient()

//TODO
// Actualización de stock y precios

async function allMovies(data) {
    console.log(data)
    const movies = await prisma.movie.findMany({
        ...{
            skip: data.page * data.size || undefined,
            take: data.size || undefined,
            where: { deleted: false, ...data.filters },
            include: {
                likes: true,
            },
            orderBy: data.sorts.title
                ? [{ ["title"]: data.sorts.title }]
                : undefined,
        },
        ...data.pagination,
    })

    // return of movies with length of likes
    //then with sorting of like
    return movies
        .map((movie) => ({
            ...movie,
            images: movie.images || [],
            likes: movie.likes.length,
        }))
        .sort((a, b) => {
            if (data.sorts && data.sorts.like) {
                return data.sorts.like == "asc"
                    ? a.likes - b.likes
                    : b.likes - a.likes
            }
            return 0
        })
}

async function filterMovies(data) {
    const filteredMovies = await prisma.movie.findMany({
        where: {
            OR: [
                { title: { contains: data.search } },
                { description: { contains: data.search } },
            ],
            deleted: false,
        },
        include: { likes: true },
    })

    return filteredMovies.map((movie) => ({
        ...movie,
        images: movie.images || [],
        likes: movie.likes.length,
    }))
}

async function listAvailableMovies(data) {
    try {
        const availableMovies = await prisma.movie.findMany({
            where: { available: { equals: true }, deleted: false },
            skip: data.page * data.size || undefined,
            take: data.size || undefined,
            include: {
                likes: true,
            },
        })
        return (
            availableMovies.map((movie) => ({
                ...movie,
                likes: movie.likes.length,
            })) || []
        )
    } catch (error) {
        console.log(error.message)
        throw new Error(error.message)
    }
}

async function getMovie(id) {
    const movie = await prisma.movie.findUnique({
        where: { id: parseInt(id) },
        include: {
            likes: true,
            rental: true,
            invoiceItems: true,
            stocks: true,
        },
    })

    return movie
}

// crear
async function upsertMovie(data) {
    try {
        const {
            title,
            description,
            realeasedDay,
            images,
            stocks,
            oldStocks,
            id,
            available,
        } = data

        const newStocks =
            stocks.map(({ id, type, stock, price, active }) => ({
                where: { id: id | null },
                update: { type, stock, price, active },
                create: { type, stock, price, active },
            })) || undefined

        const oldStocks1 = oldStocks
            ? oldStocks.map(({ id, stock }) => ({
                  where: { id: id },
                  update: { active: false, stock },
                  create: { active: false, stock },
              }))
            : []

        console.log([...newStocks, ...oldStocks1])

        const movie = await prisma.movie.upsert({
            where: { id: id | null },
            update: {
                title,
                description,
                realeasedDay: new Date(realeasedDay).toISOString(),
                images: JSON.stringify(images),
                available,
                stocks: {
                    upsert: [...newStocks, ...oldStocks1],
                },
            },
            create: {
                title,
                description,
                realeasedDay: new Date(realeasedDay).toISOString(),
                images: JSON.stringify(images),
                available,
                stocks: {
                    connectOrCreate: newStocks.map(({ where, create }) => ({
                        where,
                        create,
                    })),
                },
            },
            include: { stocks: true },
        })

        console.log(movie)
        return {
            ...movie,
            images: movie.images ? movie.images : [],
        }
    } catch (error) {
        console.log(error.message)
        throw new Error(error.message)
    }
}

async function deleteMovie(id) {
    try {
        const response = await prisma.movie.update({
            data: { deleted: false },
            where: { id: parseInt(id) },
        })

        return response
    } catch (error) {
        console.error(error)
        throw new Error(error.message)
    }
}

module.exports = {
    allMovies,
    filterMovies,
    listAvailableMovies,
    getMovie,
    upsertMovie,
    deleteMovie,
}
