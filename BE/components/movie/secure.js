const auth = require("../../security/index")

module.exports = function checkAuth(action) {
    function middleWare(req, res, next) {
        switch (action) {
            case "upsert":
                const roles = ["ADMIN"]
                auth.check.permission(req, res, roles)
                next()
                break
            case "LIST":
                console.log("middleware list")
                auth.check.role(req, res)
                next()
                break
            default:
                next()
                break
        }
    }
    return middleWare
}
