const { Router, query } = require("express")
const express = require("express")

const secure = require("./secure")
const response = require("../../network/response")
const controller = require("./index")

const router = express.Router()

function getFilterOrderPagination(req) {
    const FILTER_FOR_LIST = ["like", "title"]
    let filters = {}
    let sorts = {}
    let pagination = {}

    // get filter available

    if (req.query.available) {
        filters.available = JSON.parse(req.query.available)
    }
    // get sorting

    if (req.query.sort) {
        sorts = req.query.sort.split(",")
        sorts = sorts
            .filter((sort) => FILTER_FOR_LIST.includes(sort.replace("-", "")))
            .reduce(
                (acc, sort) => {
                    if (sort.includes("-")) {
                        acc[sort.replace("-", "")] = "desc"
                    } else {
                        acc[sort] = "asc"
                    }
                    return acc
                },

                {}
            )
    }

    // get pagination
    if (req.query.cursor) {
        pagination.skip = 1
        pagination.cursor = parseInt(req.query.cursor)
    }
    if (req.query.limit) {
        pagination.take = parseInt(req.query.limit)
    }

    return { filters, sorts, pagination }
}

router.get("/", secure("LIST"), async (req, res) => {
    try {
        let list = []
        if (req.user.role !== "ADMIN") {
            list = await controller.listAvailableMovies(req.body)
        } else {
            list = await controller.allMovies(getFilterOrderPagination(req))
        }
        response.success(req, res, list, 200)
    } catch (error) {
        console.error(error)
        response.error(req, res, error.message, 500)
    }
})

router.get("/:id", async (req, res) => {
    try {
        // console.log(req.user)
        const movie = await controller.getMovie(req.params.id)
        response.success(req, res, movie, 200)
    } catch (error) {
        response.error(req, res, error.message, 500)
    }
})
router.get("/search/:text", async (req, res) => {
    try {
        console.log
        const movie = await controller.filterMovies({ data: req.params.text })
        response.success(req, res, movie, 200)
    } catch (error) {
        response.error(req, res, error.message, 500)
    }
})

router.post("/", secure("upsert"), async (req, res) => {
    try {
        const movie = await controller.upsertMovie(req.body)
        response.success(req, res, movie, 200)
    } catch (error) {
        response.error(req, res, error.message, 500)
    }
})

router.delete("/:id", secure("upsert"), async (req, res) => {
    try {
        const movie = await controller.deleteMovie(req.params.id)
        console.log(movie)
        response.success(req, res, movie, 200)
    } catch (error) {
        response.error(req, res, error.message, 500)
    }
})

module.exports = router
