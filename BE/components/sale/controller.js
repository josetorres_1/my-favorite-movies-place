const { PrismaClient } = require("@prisma/client")

const prisma = new PrismaClient()

//Upsert invoice

async function upsertInvoice(data) {
    const {
        id,
        customerId,
        adminId,
        returned,
        invoiceItems,
        subtotal,
        tax,
        total,
    } = data

    const procesedInvoiceItems = invoiceItems.map(
        ({ movieId, qty, salePrice }) => ({
            movieId,
            qty,
            salePrice,
            totalSalePrice: parseFloat(qty * salePrice),
        })
    )

    const result = await prisma.invoice.upsert({
        include: { invoiceItems: true },
        where: { id: id | null },
        create: {
            customerId,
            adminId: adminId | 0,
            returned: false,
            subtotal,
            tax,
            invoiceItems: {
                create: procesedInvoiceItems,
            },
            total: procesedInvoiceItems.reduce(
                (acc, invoice) => acc + invoice.totalSalePrice,
                0
            ),
        },
        update: {
            returned,
        },
        include: {
            invoiceItems: {
                select: {
                    movie: {
                        select: {
                            stocks: {
                                where: {
                                    active: true,
                                    type: "SELL",
                                },
                            },
                        },
                    },
                    qty: true,
                },
            },
        },
    })

    //update stocks
    const updates = result.invoiceItems.map((item, index) => {
        return prisma.stock.update({
            where: { id: item.movie.stocks[0].id },
            data: { stock: { decrement: item.qty } },
        })
    })

    await prisma.$transaction(updates)

    return result
}

//get invoice

async function getInvoices(data) {
    const invoices = await prisma.invoice.findMany({
        include: {
            invoiceItems: true,
            customer: true,
        },
        take: data.size,
        skip: (data.size * data.page) | null,
    })
    console.log(invoices)
    return invoices
}

async function getInvoiceByUser(data, userId) {
    const rents = await prisma.invoice.findMany({
        where: {
            customer: { id: userId | null },
        },
        include: {
            invoiceItems: {
                include: {
                    movie: true,
                },
            },
            customer: true,
        },
    })

    return rents
}

//get sales

async function getInvoice(id) {
    const invoice = await prisma.invoice.findUnique({
        where: { id },
        include: {
            invoiceItems: {
                include: {
                    movie: true,
                },
            },
            customer: true,
        },
    })

    return invoice || []
}

module.exports = { upsertInvoice, getInvoices, getInvoice, getInvoiceByUser }
