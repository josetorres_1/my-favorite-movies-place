const { Router } = require("express")
const express = require("express")

const response = require("../../network/response")
const controller = require("./index")
const secure = require("./secure")

const router = express.Router()

router.get("/", secure("UPSERT"), async (req, res) => {
    try {
        const list = await controller.getInvoices(req.body)
        response.success(req, res, list, 200)
    } catch (error) {
        console.error(error);
        response.error(req, res, error.message, 500)
    }
})

router.get("/user/:id", secure("LOGGED"), async (req, res) => {
    try {
        const userId = parseInt(req.params.id)
        const list = await controller.getInvoiceByUser(req.body, userId)
        response.success(req, res, list, 200)
    } catch (error) {
        console.error(error)

        response.error(req, res, error.message, 500)
    }
})

router.get("/:id", secure("LOGGED"), async (req, res) => {
    try {
        const id = parseInt(req.params.id)
        const sale = await controller.getInvoice(id)
        response.success(req, res, sale, 200)
    } catch (error) {
        console.error(error)

        response.error(req, res, error.message, 500)
    }
})

router.post("/", secure("LOGGED"), async (req, res) => {
    try {
        const sale = await controller.upsertInvoice(req.body)
        response.success(req, res, sale, 201)
    } catch (error) {
        console.error(error)
        response.error(req, res, error.message, 500)
    }
})

module.exports = router
